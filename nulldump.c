#include <linux/module.h>     /* Для всех модулей */
#include <linux/kernel.h>     /* KERN_INFO */
#include <linux/init.h>       /* Макросы */
#include <linux/fs.h>         /* Макросы для устройств */
#include <linux/cdev.h>	      /* Функции регистрации символьных устройств */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Author");
MODULE_DESCRIPTION("Hello, world - parameters callback error");
MODULE_VERSION("0.1");

static ssize_t nulldump_read(struct file *file, char __user *buf, size_t len, loff_t *off)
{
	pr_info("nulldump read, process: %d, command: %s\n", current->pid, current->comm);
	return 0;
}

static ssize_t nulldump_write(struct file *file, const char __user *buf, size_t len, loff_t *off)
{
	pr_info("nulldump write, process: %d, command: %s, hexdump: ", current->pid, current->comm);
	print_hex_dump(KERN_INFO, "", DUMP_PREFIX_ADDRESS, 16, 4, buf, len, 1);
	return len;
}

static struct file_operations nulldump_ops =
{
	.owner      = THIS_MODULE,
	.read       = nulldump_read,
	.write      = nulldump_write,
};

dev_t dev = 0;
static struct cdev nulldump_cdev;
static struct class *nulldump_class;

static int __init nulldump_start(void)
{
	int res;

	if ((res = alloc_chrdev_region(&dev, 0, 1, "nulldump")) < 0)
	{
		pr_err("Error allocating major number\n");
		return res;
	}
	pr_info("nulldump load: Major = %d Minor = %d\n", MAJOR(dev), MINOR(dev));
        
	cdev_init (&nulldump_cdev, &nulldump_ops);        
	if ((res = cdev_add (&nulldump_cdev, dev, 1)) < 0)
        {
		pr_err("nulldump: device registering error\n");
		unregister_chrdev_region (dev, 1);
		return res;
	}        
        
	if (IS_ERR(nulldump_class = class_create (THIS_MODULE, "nulldump_class")))
	{
		cdev_del (&nulldump_cdev);
		unregister_chrdev_region (dev, 1);
		return -1;
	}
	
	if (IS_ERR(device_create(nulldump_class, NULL, dev, NULL, "nulldump_device")))
	{
		pr_err("nulldump: error creating device\n");
		class_destroy (nulldump_class);
		cdev_del (&nulldump_cdev);
		unregister_chrdev_region(dev, 1);
		return -1;
	}
        
        return 0;
}

static void __exit nulldump_end(void)
{
	device_destroy (nulldump_class, dev);
	class_destroy (nulldump_class);
	cdev_del (&nulldump_cdev);
	unregister_chrdev_region(dev, 1);
	pr_info("nulldump: unload\n");
}

module_init(nulldump_start);
module_exit(nulldump_end);

